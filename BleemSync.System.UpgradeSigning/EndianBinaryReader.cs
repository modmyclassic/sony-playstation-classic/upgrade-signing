﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace GMWare.IO
{
    /// <summary>
    /// Reads primitive data types as binary values in a specific encoding with a choice of endianess.
    /// </summary>
    public class EndianBinaryReader : BinaryReader
    {
        byte[] buffer;

        /// <summary>
        /// Gets or sets a value that indicates the endianess of the data to read.
        /// </summary>
        public Endianess Endianess { get; set; }

        /// <summary>
        /// Initializes a new instance of the EndianBinaryReader class based on the specified stream and using UTF-8 encoding.
        /// </summary>
        /// <param name="input">The input stream.</param>
        public EndianBinaryReader(Stream input)
            : base(input)
        { }

        /// <summary>
        /// Initializes a new instance of the EndianBinaryReader class based on the specified stream and character encoding.
        /// </summary>
        /// <param name="input">The input stream.</param>
        /// <param name="encoding">The character encoding to use.</param>
        public EndianBinaryReader(Stream input, Encoding encoding)
            : base(input, encoding)
        { }

        void readAndReverse(int count)
        {
            buffer = ReadBytes(count);
            if (buffer.Length != count) throw new EndOfStreamException();
            Array.Reverse(buffer);
        }

        /// <summary>
        /// Reads an 8-byte floating point value from the current stream and advances the current position of the stream by eight bytes.
        /// </summary>
        /// <returns>An 8-byte floating point value read from the current stream.</returns>
        public override double ReadDouble()
        {
            if (Endianess == Endianess.LittleEndian) return base.ReadDouble();
            readAndReverse(sizeof(double));
            return BitConverter.ToDouble(buffer, 0);
        }

        /// <summary>
        /// Reads a 2-byte signed integer from the current stream and advances the current position of the stream by two bytes.
        /// </summary>
        /// <returns>A 2-byte signed integer read from the current stream.</returns>
        public override short ReadInt16()
        {
            if (Endianess == Endianess.LittleEndian) return base.ReadInt16();
            readAndReverse(sizeof(short));
            return BitConverter.ToInt16(buffer, 0);
        }

        /// <summary>
        /// Reads a 4-byte signed integer from the current stream and advances the current position of the stream by four bytes.
        /// </summary>
        /// <returns>A 4-byte signed integer read from the current stream.</returns>
        public override int ReadInt32()
        {
            if (Endianess == Endianess.LittleEndian) return base.ReadInt32();
            readAndReverse(sizeof(int));
            return BitConverter.ToInt32(buffer, 0);
        }

        /// <summary>
        /// Reads an 8-byte signed integer from the current stream and advances the current position of the stream by eight bytes.
        /// </summary>
        /// <returns>An 8-byte signed integer read from the current stream.</returns>
        public override long ReadInt64()
        {
            if (Endianess == Endianess.LittleEndian) return base.ReadInt64();
            readAndReverse(sizeof(long));
            return BitConverter.ToInt64(buffer, 0);
        }

        /// <summary>
        /// Reads a 4-byte floating point value from the current stream and advances the current position of the stream by four bytes.
        /// </summary>
        /// <returns>A 4-byte floating point value read from the current stream.</returns>
        public override float ReadSingle()
        {
            if (Endianess == Endianess.LittleEndian) return base.ReadSingle();
            readAndReverse(sizeof(float));
            return BitConverter.ToSingle(buffer, 0);
        }

        /// <summary>
        /// Reads a 2-byte unsigned integer from the current stream using little-endian encoding and advances the position of the stream by two bytes.
        /// </summary>
        /// <returns>A 2-byte unsigned integer read from this stream.</returns>
        public override ushort ReadUInt16()
        {
            if (Endianess == Endianess.LittleEndian) return base.ReadUInt16();
            readAndReverse(sizeof(ushort));
            return BitConverter.ToUInt16(buffer, 0);
        }

        /// <summary>
        /// Reads a 4-byte unsigned integer from the current stream and advances the position of the stream by four bytes.
        /// </summary>
        /// <returns>A 4-byte unsigned integer read from this stream.</returns>
        public override uint ReadUInt32()
        {
            if (Endianess == Endianess.LittleEndian) return base.ReadUInt32();
            readAndReverse(sizeof(uint));
            return BitConverter.ToUInt32(buffer, 0);
        }

        /// <summary>
        /// Reads an 8-byte unsigned integer from the current stream and advances the position of the stream by eight bytes.
        /// </summary>
        /// <returns>An 8-byte unsigned integer read from this stream.</returns>
        public override ulong ReadUInt64()
        {
            if (Endianess == Endianess.LittleEndian) return base.ReadUInt64();
            readAndReverse(sizeof(ulong));
            return BitConverter.ToUInt64(buffer, 0);
        }
    }

    /// <summary>
    /// Specifies constants defining endianess.
    /// </summary>
    public enum Endianess
    {
        /// <summary>
        /// Data is little endian.
        /// </summary>
        LittleEndian,
        /// <summary>
        /// Data is big endian.
        /// </summary>
        BigEndian
    }
}
