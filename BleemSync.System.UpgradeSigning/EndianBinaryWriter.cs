﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace GMWare.IO
{
    /// <summary>
    /// Writes primitive types in binary to a stream and supports writing strings in a specific encoding with a choice of endianess.
    /// </summary>
    public class EndianBinaryWriter : BinaryWriter
    {
        /// <summary>
        /// Gets or sets a value that indicates the endianess of the data to write.
        /// </summary>
        public Endianess Endianess { get; set; }

        /// <summary>
        /// Initializes a new instance of the EndianBinaryWriter class based on the specified stream and using UTF-8 encoding.
        /// </summary>
        /// <param name="input">The output stream.</param>
        public EndianBinaryWriter(Stream input)
            : base(input)
        { }

        /// <summary>
        /// Initializes a new instance of the EndianBinaryWriter class based on the specified stream and character encoding.
        /// </summary>
        /// <param name="input">The output stream.</param>
        /// <param name="encoding">The character encoding to use.</param>
        public EndianBinaryWriter(Stream input, Encoding encoding)
            : base(input, encoding)
        { }

        void reverseAndWrite(byte[] buffer)
        {
            Array.Reverse(buffer);
            Write(buffer);
        }

        /// <summary>
        /// Writes an eight-byte floating-point value to the current stream and advances the stream position by eight bytes.
        /// </summary>
        /// <param name="value">The eight-byte floating-point value to write.</param>
        public override void Write(double value)
        {
            if (Endianess == Endianess.LittleEndian) base.Write(value);
            else reverseAndWrite(BitConverter.GetBytes(value));
        }

        /// <summary>
        /// Writes a four-byte floating-point value to the current stream and advances the stream position by four bytes.
        /// </summary>
        /// <param name="value">The four-byte floating-point value to write.</param>
        public override void Write(float value)
        {
            if (Endianess == Endianess.LittleEndian) base.Write(value);
            else reverseAndWrite(BitConverter.GetBytes(value));
        }

        /// <summary>
        /// Writes a four-byte signed integer to the current stream and advances the stream position by four bytes.
        /// </summary>
        /// <param name="value">The four-byte signed integer to write.</param>
        public override void Write(int value)
        {
            if (Endianess == Endianess.LittleEndian) base.Write(value);
            else reverseAndWrite(BitConverter.GetBytes(value));
        }

        /// <summary>
        /// Writes an eight-byte signed integer to the current stream and advances the stream position by eight bytes.
        /// </summary>
        /// <param name="value">The eight-byte signed integer to write.</param>
        public override void Write(long value)
        {
            if (Endianess == Endianess.LittleEndian) base.Write(value);
            else reverseAndWrite(BitConverter.GetBytes(value));
        }

        /// <summary>
        /// Writes a two-byte signed integer to the current stream and advances the stream position by two bytes.
        /// </summary>
        /// <param name="value">The two-byte signed integer to write.</param>
        public override void Write(short value)
        {
            if (Endianess == Endianess.LittleEndian) base.Write(value);
            else reverseAndWrite(BitConverter.GetBytes(value));
        }

        /// <summary>
        /// Writes a four-byte unsigned integer to the current stream and advances the stream position by four bytes.
        /// </summary>
        /// <param name="value">The four-byte unsigned integer to write.</param>
        public override void Write(uint value)
        {
            if (Endianess == Endianess.LittleEndian) base.Write(value);
            else reverseAndWrite(BitConverter.GetBytes(value));
        }

        /// <summary>
        /// Writes an eight-byte unsigned integer to the current stream and advances the stream position by eight bytes.
        /// </summary>
        /// <param name="value">The eight-byte unsigned integer to write.</param>
        public override void Write(ulong value)
        {
            if (Endianess == Endianess.LittleEndian) base.Write(value);
            else reverseAndWrite(BitConverter.GetBytes(value));
        }

        /// <summary>
        /// Writes a two-byte unsigned integer to the current stream and advances the stream position by two bytes.
        /// </summary>
        /// <param name="value">The two-byte unsigned integer to write.</param>
        public override void Write(ushort value)
        {
            if (Endianess == Endianess.LittleEndian) base.Write(value);
            else reverseAndWrite(BitConverter.GetBytes(value));
        }
    }
}
