﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Engines;
using Org.BouncyCastle.Crypto.Parameters;


namespace BleemSync.System.UpgradeSigning
{
    /// <summary>
    /// Representation of the signature field within a RAWC.
    /// </summary>
    public class RsaSignature
    {
        byte[] data;
        bool decrypted;

        /// <summary>
        /// Gets the padding before the hash. The signature must be decrypted first.
        /// </summary>
        public byte[] PrePadding
        {
            get
            {
                if (!decrypted) throw new InvalidOperationException("Signature has not been decrypted.");
                byte[] buf = new byte[0x10];
                Buffer.BlockCopy(data, 0, buf, 0, buf.Length);
                return buf;
            }
        }
        /// <summary>
        /// Gets or sets the hash of the chunks and header. The signature must be decrypted first.
        /// </summary>
        public byte[] Hash
        {
            get
            {
                if (!decrypted) throw new InvalidOperationException("Signature has not been decrypted.");
                byte[] buf = new byte[0x20];
                Buffer.BlockCopy(data, 0x10, buf, 0, buf.Length);
                return buf;
            }
            set
            {
                if (!decrypted) throw new InvalidOperationException("Signature has not been decrypted.");
                if (value == null || value.Length != 0x20) throw new ArgumentException("Value is null or incorrect length.", nameof(value));
                Buffer.BlockCopy(value, 0, data, 0x10, value.Length);
            }
        }
        /// <summary>
        /// Gets the padding after the hash. The signature must be decrypted first.
        /// </summary>
        public byte[] PostPadding
        {
            get
            {
                if (!decrypted) throw new InvalidOperationException("Signature has not been decrypted.");
                byte[] buf = new byte[data.Length - 0x30];
                Buffer.BlockCopy(data, 0x30, buf, 0, buf.Length);
                return buf;
            }
        }
        /// <summary>
        /// Gets whether the signature has been decrypted.
        /// </summary>
        public bool IsDecrypted
        {
            get
            {
                return decrypted;
            }
        }

        /// <summary>
        /// Instantiates a new signature for loading a hash and encrypting.
        /// </summary>
        public RsaSignature()
        {
            data = new byte[0x200];
            decrypted = true;
            using (RandomNumberGenerator rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(data);
            }
            data[0] &= 0x7f; // Make sure data isn't too big for modulus (reduce the bit length by 1)
        }

        /// <summary>
        /// Instantiates a new signature for decrypting and reading.
        /// </summary>
        /// <param name="data">The encrypted signature blob.</param>
        public RsaSignature(byte[] data)
        {
            if (data == null || data.Length != 0x200) throw new ArgumentException("Buffer null or incorrect length.", nameof(data));
            this.data = data;
        }

        /// <summary>
        /// Gets the signature data buffer in its current decryption state.
        /// </summary>
        /// <returns></returns>
        internal byte[] GetBuffer()
        {
            return (byte[])data.Clone();
        }

        /// <summary>
        /// Decrypts the signature blob.
        /// </summary>
        /// <param name="key">The key used to encrypt the signature.</param>
        public void Decrypt(RsaKeyParameters key)
        {
            if (decrypted) return;
            var engine = new RsaEngine();
            engine.Init(false, key);
            data = engine.ProcessBlock(data, 0, data.Length);
            decrypted = true;
        }

        /// <summary>
        /// Encrypts the signature blob.
        /// </summary>
        /// <param name="key">The key to use to encrypt the signature.</param>
        public void Encrypt(RsaPrivateCrtKeyParameters key)
        {
            if (!decrypted) return;
            var engine = new RsaEngine();
            engine.Init(true, key);
            data = engine.ProcessBlock(data, 0, data.Length);
            decrypted = false;
        }
    }
}
