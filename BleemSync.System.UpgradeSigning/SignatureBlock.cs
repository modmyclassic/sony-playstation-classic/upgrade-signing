﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using GMWare.IO;

namespace BleemSync.System.UpgradeSigning
{
    /// <summary>
    /// A representation of the RAWC signature block.
    /// </summary>
    public class SignatureBlock
    {
        static readonly char[] MagicValue = { 'R', 'A', 'W', 'C' };

        /// <summary>
        /// The 4-char magic ID of this block.
        /// </summary>
        public char[] Magic { get; set; }
        /// <summary>
        /// A 2-byte gap, currently used as a version indicator.
        /// </summary>
        public ushort Version { get; set; }
        /// <summary>
        /// The size of a chunk to hash.
        /// </summary>
        public long ChunkSize { get; set; }
        /// <summary>
        /// The number of bytes to skip between chunks to hash.
        /// </summary>
        public long ChunkGap { get; set; }
        /// <summary>
        /// The number of chunks to hash.
        /// </summary>
        public long ChunkCount { get; set; }
        /// <summary>
        /// A 32-byte field, currently used as a comment field.
        /// </summary>
        public string SignedComment { get; set; }
        /// <summary>
        /// The RSA signature of the chunks and header.
        /// </summary>
        public RsaSignature Signature { get; set; }
        /// <summary>
        /// Blank space remaining in the signature block's sector.
        /// </summary>
        public byte[] PostPadding { get; set; }

        public SignatureBlock()
        {
            Magic = (char[])MagicValue.Clone();
        }

        /// <summary>
        /// Gets the signed area of the block as bytes.
        /// </summary>
        /// <returns></returns>
        public byte[] GetSignedAreaAsBytes()
        {
            using (MemoryStream ms = new MemoryStream())
            {
                EndianBinaryWriter bw = new EndianBinaryWriter(ms) { Endianess = Endianess.BigEndian };

                if (Magic == null || Magic.Length != 4) throw new InvalidOperationException("Magic value is null or wrong length.");
                bw.Write(Magic);
                bw.Write(Version);
                bw.Write(ChunkSize);
                bw.Write(ChunkGap);
                bw.Write(ChunkCount);

                if (SignedComment != null)
                {
                    byte[] commentBytes = Encoding.UTF8.GetBytes(SignedComment);
                    if (commentBytes.Length > 0x1f) throw new InvalidOperationException("Comment is too long.");
                    Array.Resize(ref commentBytes, 0x20);
                    bw.Write(commentBytes);
                }
                else
                {
                    bw.Write(new byte[0x20]);
                }

                ms.Flush();
                return ms.ToArray();
            }
        }

        /// <summary>
        /// Write signature to stream.
        /// </summary>
        /// <param name="stream">The stream to write this signature block to.</param>
        public void Write(Stream stream)
        {
            if (stream == null) throw new ArgumentNullException(nameof(stream));
            if (Signature == null) throw new InvalidOperationException("No signature present.");
            if (Signature.IsDecrypted) throw new InvalidOperationException("Signature needs to be encrypted first.");

            EndianBinaryWriter bw = new EndianBinaryWriter(stream) { Endianess = Endianess.BigEndian };
            bw.Write(GetSignedAreaAsBytes());
            bw.Write(Signature.GetBuffer());
            if (PostPadding != null)
            {
                byte[] padding = PostPadding;
                Array.Resize(ref padding, 0xdc2);
                bw.Write(padding);
            }
            else
            {
                bw.Write(new byte[0xdc2]);
            }
        }

        /// <summary>
        /// Read signature block from stream.
        /// </summary>
        /// <param name="stream">The stream to read from.</param>
        /// <returns>The signature block read.</returns>
        public static SignatureBlock Read(Stream stream)
        {
            if (stream == null) throw new ArgumentNullException(nameof(stream));
            EndianBinaryReader br = new EndianBinaryReader(stream) { Endianess = Endianess.BigEndian };
            SignatureBlock block = new SignatureBlock();
            char[] magic = br.ReadChars(4);
            if (!MagicValue.SequenceEqual(magic)) throw new InvalidDataException("Data is not a RAWC block.");
            block.Magic = magic;
            block.Version = br.ReadUInt16();
            block.ChunkSize = br.ReadInt64();
            block.ChunkGap = br.ReadInt64();
            block.ChunkCount = br.ReadInt64();

            byte[] comment = br.ReadBytes(0x20);
            // Determine length a la C string
            int l = 0;
            for (l = 0; l < 0x20; ++l)
            {
                if (comment[l] == 0) break;
            }
            block.SignedComment = Encoding.UTF8.GetString(comment, 0, l);

            block.Signature = new RsaSignature(br.ReadBytes(0x200));
            block.PostPadding = br.ReadBytes(0xdc2);

            return block;
        }
    }
}
