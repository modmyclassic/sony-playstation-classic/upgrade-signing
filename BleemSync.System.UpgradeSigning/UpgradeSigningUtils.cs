﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Security.Cryptography;
using Org.BouncyCastle.Crypto.Parameters;

namespace BleemSync.System.UpgradeSigning
{
    public static class UpgradeSigningUtils
    {
        /// <summary>
        /// Verifies whether a signed file is valid given the key it's signed with.
        /// </summary>
        /// <param name="stream">The stream of the file to verify. Must be seekable.</param>
        /// <param name="key">The key used to sign the file.</param>
        /// <param name="block">The signature block read from the file.</param>
        /// <param name="isBlockAppended">True if block is after file contents, false if block is before file contents.</param>
        /// <returns>Whether the file's signature is valid.</returns>
        public static bool VerifyFile(Stream stream, RsaKeyParameters key, out SignatureBlock block, out bool isBlockAppended)
        {
            if (stream == null) throw new ArgumentNullException(nameof(stream));
            if (!stream.CanSeek) throw new ArgumentException("Stream is not seekable.", nameof(stream));
            if (stream.Length < 0x1000) throw new ArgumentException("Stream is too short.", nameof(stream));

            // Try at the beginning of the file
            try
            {
                stream.Seek(0, SeekOrigin.Begin);
                block = SignatureBlock.Read(stream);
                isBlockAppended = false;
            }
            catch (InvalidDataException)
            {
                // Try at the end of the file
                try
                {
                    stream.Seek(-0x1000, SeekOrigin.End);
                    block = SignatureBlock.Read(stream);
                    stream.Seek(0, SeekOrigin.Begin);
                    isBlockAppended = true;
                }
                catch (InvalidDataException)
                {
                    throw new ArgumentException("Stream does not contain a signature.", nameof(stream));
                }
            }

            // Length validation
            if (stream.Length - 0x1000 >= 0x80000000)
            {
                if ((block.ChunkSize + block.ChunkGap) * block.ChunkCount - block.ChunkGap >= 0x80000000)
                {
                    throw new ArgumentException("Stream too long to validate.", nameof(stream));
                }
            }

            byte[] hash = CalculateHash(stream, block.GetSignedAreaAsBytes(), block.ChunkSize, block.ChunkGap, block.ChunkCount);
            block.Signature.Decrypt(key);
            return hash.SequenceEqual(block.Signature.Hash);
        }

        /// <summary>
        /// Signs a file given params from a signature block and the signing key.
        /// </summary>
        /// <param name="inStream">The stream of the file to sign. Must be seekable.</param>
        /// <param name="outStream">The output stream.</param>
        /// <param name="block">The signature block to use in signing.</param>
        /// <param name="key">The key to sign the file with.</param>
        /// <param name="appendBlock">True to write the signature block to the end of the file, false to write before.</param>
        public static void SignFile(Stream inStream, Stream outStream, SignatureBlock block, RsaPrivateCrtKeyParameters key, bool appendBlock)
        {
            if (inStream == null) throw new ArgumentNullException(nameof(inStream));
            if (!inStream.CanSeek) throw new ArgumentException("Stream is not seekable.", nameof(inStream));
            if (outStream == null) throw new ArgumentNullException(nameof(outStream));
            if (block == null) throw new ArgumentNullException(nameof(block));

            // Length validation
            long validateLength = (block.ChunkSize + block.ChunkGap) * block.ChunkCount - block.ChunkGap;
            if (validateLength >= 0x80000000) throw new ArgumentException("Validation length calculated from chunk configuration is too long.", nameof(block));
            if (validateLength > inStream.Length) throw new ArgumentException("Chunk configuration results in validation length too long for input.", nameof(block));

            inStream.Seek(0, SeekOrigin.Begin);
            byte[] hash = CalculateHash(inStream, block.GetSignedAreaAsBytes(), block.ChunkSize, block.ChunkGap, block.ChunkCount);
            block.Signature = new RsaSignature();
            block.Signature.Hash = hash;
            block.Signature.Encrypt(key);

            if (!appendBlock)
            {
                block.Write(outStream);
            }
            inStream.Seek(0, SeekOrigin.Begin);
            inStream.CopyTo(outStream);
            if (appendBlock)
            {
                block.Write(outStream);
            }
        }

        /// <summary>
        /// Calculates hash of stream and extra data.
        /// </summary>
        /// <param name="stream">The stream to hash.</param>
        /// <param name="additional">The final block of data to hash.</param>
        /// <param name="chunkSize">The length of a chunk.</param>
        /// <param name="chunkGap">The number of bytes to skip after hashing one chunk.</param>
        /// <param name="chunkCount">The number of chunks to hash.</param>
        /// <returns>The computed hash.</returns>
        static byte[] CalculateHash(Stream stream, byte[] additional, long chunkSize, long chunkGap, long chunkCount)
        {
            using (SHA256 sha = SHA256.Create())
            {
                sha.Initialize();
                byte[] buffer = new byte[chunkSize];
                for (int i = 0; i < chunkCount; ++i)
                {
                    if (stream.Read(buffer, 0, buffer.Length) != buffer.Length)
                    {
                        throw new IOException("Failed to read required number of bytes.");
                    }
                    sha.TransformBlock(buffer, 0, buffer.Length, buffer, 0);
                    stream.Seek(chunkGap, SeekOrigin.Current);
                }
                sha.TransformFinalBlock(additional, 0, additional.Length);
                return sha.Hash;
            }
        }
    }
}
