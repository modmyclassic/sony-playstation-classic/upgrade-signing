﻿using System;
using System.IO;
using McMaster.Extensions.CommandLineUtils;
using BleemSync.System.UpgradeSigning;

namespace UpgradeSign
{
    class Program
    {
        static void Main(string[] args)
        {
            var app = new CommandLineApplication
            {
                FullName = "PlayStation Classic Upgrade Signer"
            };

            app.Command("sign", (config) =>
            {
                config.FullName = "Sign File";
                config.Description = "Sign a file";
                var filePathArg = config.Argument("filePath", "Path of the signed file to verify").IsRequired();
                filePathArg.Accepts().ExistingFile();
                var outPathArg = config.Argument("outPath", "Path to write signed file to").IsRequired();
                var keyPathArg = config.Argument("keyPath", "Path of the PEM formatted private key file to sign with").IsRequired();
                keyPathArg.Accepts().ExistingFile();
                var chunkSizeOpt = config.Option<long>("-s|--chunk-size", "Tuning: The size of a chunk to hash", CommandOptionType.SingleValue);
                var chunkGapOpt = config.Option<long>("-g|--chunk-gap", "Tuning: The number of bytes to skip after a chunk", CommandOptionType.SingleValue);
                var chunkCountOpt = config.Option<long>("-n|--chunk-count", "Tuning: The number of chunks to hash", CommandOptionType.SingleValue);
                var commentOpt = config.Option("-c|--comment", "Comment to add to signature block", CommandOptionType.SingleValue);
                var appendOpt = config.Option("-a|--append-sig", "Append signature to output instead of prepending", CommandOptionType.NoValue);
                commentOpt.Accepts().MaxLength(0x1f);
                config.HelpOption();

                config.OnExecute(() =>
                {
                    long chunkSize = 0;
                    long chunkGap = 0;
                    long chunkCount = 1;
                    bool append = appendOpt.HasValue();

                    if (chunkSizeOpt.HasValue())
                    {
                        if (chunkSizeOpt.ParsedValue < 0) throw new ArgumentOutOfRangeException("chunk-size", "Chunk size must not be negative.");
                        chunkSize = chunkSizeOpt.ParsedValue;
                    }
                    if (chunkGapOpt.HasValue())
                    {
                        if (chunkGapOpt.ParsedValue < 0) throw new ArgumentOutOfRangeException("chunk-gap", "Chunk gap must not be negative.");
                        chunkGap = chunkGapOpt.ParsedValue;
                    }
                    if (chunkCountOpt.HasValue())
                    {
                        if (chunkCountOpt.ParsedValue < 0) throw new ArgumentOutOfRangeException("chunk-count", "Chunk count must not be negative.");
                        chunkCount = chunkCountOpt.ParsedValue;
                    }

                    var key = RsaPem.PrivateKeyFromPemFile(keyPathArg.Value);

                    using (FileStream fs = File.OpenRead(filePathArg.Value))
                    using (FileStream os = File.Create(outPathArg.Value))
                    {
                        if (!chunkSizeOpt.HasValue()) chunkSize = fs.Length;
                        if (!chunkCountOpt.HasValue() && chunkSize + chunkGap > 0)
                        {
                            // Calculate closest chunk count to cover entire file
                            chunkCount = (fs.Length + chunkGap) / (chunkSize + chunkGap);
                        }
                        SignatureBlock block = new SignatureBlock
                        {
                            Version = 1,
                            ChunkSize = chunkSize,
                            ChunkGap = chunkGap,
                            ChunkCount = chunkCount
                        };
                        if (commentOpt.HasValue()) block.SignedComment = commentOpt.Value();
                        UpgradeSigningUtils.SignFile(fs, os, block, key, append);
                    }
                });
            });

            app.Command("verify", (config) =>
            {
                config.FullName = "Verify File";
                config.Description = "Verify a signed file";
                var filePathArg = config.Argument("filePath", "Path of the signed file to verify").IsRequired();
                filePathArg.Accepts().ExistingFile();
                var keyPathArg = config.Argument("keyPath", "Path of the PEM formatted public key file to verify with").IsRequired();
                keyPathArg.Accepts().ExistingFile();
                config.HelpOption();

                config.OnExecute(() =>
                {
                    var key = RsaPem.PublicKeyFromPemFile(keyPathArg.Value);
                    SignatureBlock block;
                    bool isAppended;
                    using (FileStream fs = File.OpenRead(filePathArg.Value))
                    {
                        if (UpgradeSigningUtils.VerifyFile(fs, key, out block, out isAppended))
                        {
                            Console.WriteLine("File signature valid.");
                        }
                        else
                        {
                            Console.WriteLine("File signature invalid.");
                            Environment.ExitCode = -1;
                        }
                    }
                });
            });

            app.Command("generate-key", (config) =>
            {
                config.FullName = "Generate Key Pair";
                config.Description = "Generate a key pair in PEM format for signing and verification";
                var publicPathArg = config.Argument("publicPath", "Path of the public key PEM to save to").IsRequired();
                var privatePathArg = config.Argument("privatePath", "Path of the private key PEM to save to").IsRequired();
                config.HelpOption();

                config.OnExecute(() =>
                {
                    RsaPem.GenerateRsaKeyPair(privatePathArg.Value, publicPathArg.Value);
                });
            });

            app.VersionOptionFromAssemblyAttributes(System.Reflection.Assembly.GetExecutingAssembly());
            app.HelpOption();

            app.OnExecute(() =>
            {
                app.ShowHelp();
                return 1;
            });

            try
            {
                app.Execute(args);
            }
            catch (CommandParsingException ex)
            {
                Console.Error.WriteLine(ex.Message);
                Environment.Exit(1);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Error while processing: {0}", ex);
                Environment.Exit(-1);
            }
        }
    }
}
