PlayStation Classic Upgrade System
==================================

The PlayStation Classic has a built-in upgrader. This document outlines how it
works, and introduces a tool for signing files.

`upgrade_app`
=============
`upgrade_app` is the built-in upgrade flasher on the PlayStation Classic. It
reads an upgrade file from USB, verifies it, and flashes it. In normal boot,
it needs to be called to start the upgrade process. In recovery boot, it is
automatically started and polls for files on an attached USB drive to flash
the normal system. The program takes a single argument as a command. They are:

- `normal`: Reboots the system to recovery and waits to flash a normal system.
  It will refuse to boot into recovery if it detects recovery is not successfully
  flashed previously. After flashing completes successfully, the system is
  rebooted back to normal.
- `recovery`: Flashes the recovery system. A flag will be set before flashing
  to make sure the program won't reboot into a partially flashed recovery system.

Upgrade packages
----------------
Once `upgrade_app` runs, it mounts `/dev/sda1` to `/dev/sdd1` and `/dev/sda` to `/dev/sdd`
into the mountpoints `/run/media/sda1` to `/run/media/sdd1` and `/run/media/sda`
to `/run/media/sdd`. After each mounted the program looks for
upgrade packages at the root of the drive. Drives are not unmounted if the search
fails.

### Normal files

#### `LBOOT.EPB`: normal system package
This package contains files to be flashed to a normal system.

- `boot.img`: Normal kernel image, flashes to `BOOTIMG1` partition
- `rootfs.ext4`: Normal system rootfs, flashes to `ROOTFS1` partition
- `tz.img`: Normal system Trusted Execution Environment image, flashes to `TEE1`
  partition
- `userdata.ext4`: User data filesystem, flashes to `USRDATA` partition

#### `LDATA.EPB`: game data image
This file is the raw image written to the `GAADATA` partition.

One or both of `LBOOT.EPB` or `LDATA.EPB` can be flashed at the same time.

### Recovery files

#### `LRECOVERY.EPB`: recovery system package
This package contains files to be flashed to the recovery system.

- `boot.img`: Recovery kernel image, flashes to `BOOTIMG2` partition
- `recovery.ext4`: Recovery system rootfs, flashes to `ROOTFS2` partition
- `tz.img`: Recovery system Trusted Execution Environment image, flashes to `TEE2`
  partition

`LBOOT.EPB` and `LRECOVERY.EPB` are both plain ZIP files, with all files included
stored at the root of the ZIP file. If a partition does not need to be flashed,
the file can be omitted. Files are flashed in the order listed, and if one file
fails to flash, the remaining files will not be flashed.

When files are being flashed, only as much data as provided will be written.
The partitions are not erased, so it is possible to flash 0-byte files.

LED statuses
------------
- Flashing green: the program is looking for upgrade packages, or currently
  flashing the system.
- Solid green: the program has successfully completed flashing.
- Flashing red: the program encountered an error.

Exit codes
----------
- -1: an error has occurred
- 0: flashing was successful
- 1: command unknown
- -2: no USB drives found
- -3: drives mounted but file not found
- -255: mounting error

File signing and `image_verify_tool`
====================================
Every upgrade package is signed, and this signature is checked before flashing.
`upgrade_app` will not flash unsigned packages. To perform signature checking,
`upgrade_app` calls `image_verify_tool`.

Usage
-----
```
image_verify_tool <verify file path> <result file path> [clean file path]
```

- verify file path: the path of the file to verify
- result file path: the path to write result file to
- clean file path: the path to write file with signature block stripped to

Result file path is `/tmp/verify_result.txt` when called by `upgrade_app`. The
clean file path is only provided for `LBOOT.EPB` and `LRECOVERY.EPB`, at
`/tmp/BOOT.BPB`. `LDATA.EPB` is not cleaned because presumably it's too large
to fit in `/tmp`, which is backed by RAM.

Signature block
---------------
The signature block is a 4KiB block of data located either at the beginning or
the end of the signed file. It has the following format:

```c
struct RAWC { // Numbers are in big endian
/* 0x0000 */    char signature[4]; // "RAWC"
/* 0x0004 */    short padding; // Could be version or something else, not referenced
/* 0x0006 */    long long chunk_size;
/* 0x000e */    long long chunk_gap; // Number of bytes to skip between chunks when hashing
/* 0x0016 */    long long chunk_count; // Number of chunks to validate
/* 0x001e */    char random[0x20]; // Some padding
/* 0x003e */    union {
                    char bytes[0x200]; // Simply RSA-4096 encrypted blob, no padding
                    struct {
                        char pre_padding[0x10]; // Some padding, could be random
/* 0x004e */            char hash[0x20];  // SHA256 hash of chunks and RAWC prior to signature
/* 0x006e */            char post_padding[0x1d0]; // Rest of the bytes of the signature, can be random
                    } content;
                } signature;
/* 0x023e */    char padding[0xdc2]; // Rest of the RAWC block, unverified
};
```

Signing is performed by hashing chunks of data and finally, the signed portion
of the signature block (the first 62 bytes). The program will refuse to process
any signed region that is 2GiB or greater in length. The signed region size is
calculated as follows:

```
(chunk_size + chunk_gap) * chunk_count - chunk_gap
```
Note the last `chunk_gap` is subtracted because this region is not checked.

RSA encryption
--------------
Signature is verified by decrypting the RSA blob within the signature block,
and comparing the computed hash against the decrypted hash from the blob.
The blob uses no padding and is decrypted with the public key (similar to a
normal RSA signature). The key used is hardcoded to `/etc/upg_pubk.pem`.

Result file
-----------
The result file consists of three integers:
```c
struct result {
    int return_value; // 0 = success, -1 = error
    int is_sig_at_end; // 1 if signature at the end, 0 if signature at the beginning
    int signature_length; // currently 0x1000
};
```

The program will not produce a cleaned file if signature verification failed.

`UpgradeSign`
=============
`UpgradeSign` is a program created to generate the signature as expected by
`image_verify_tool`.

Generating keypair
------------------
The program can generate a 4096-bit RSA keypair in PEM format for signing and
verification. Run the program with the `-?` argument to see available options.

About chunk parameters
----------------------
When choosing chunk parameters, you have to balance between performance and
security. You can check every single byte, but for large files, it might take
too long to do so. In addition, if your file is not an exact multiple of the
chunk size, some of the data will remain unverified and is subject to manipulation.
I haven't done any benchmarking, so I don't have any good advice for you right
now, but as a default, the chunk size is the same as the file. If your file
goes above 100MB, you will probably want to switch the chunk size to something
smaller and add in a gap. If you set the chunk size and gap, the program will
automatically calculate the count to cover as much of the file as possible.
